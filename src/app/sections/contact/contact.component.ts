import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { init, send } from 'emailjs-com';
import { CruzineService } from 'src/app/services/cruzine.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup = this.fb.group({
    email: this.fb.control('', [Validators.required, Validators.email]),
    message: this.fb.control('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(1000),
    ]),
  });

  constructor(
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private cruzineService: CruzineService
  ) {}

  ngOnInit(): void {}

  async sendMessage() {
    if (this.contactForm.status === 'INVALID') {
      this.openSnackBar();
      return;
    }
    this.cruzineService.openLoadingDialog();
    const { email, message } = this.contactForm.value;
    const response = await this.sendEmail(email, message);
    this.cruzineService.closeLoadingDialog();
    if (response.status === 200) {
      this.cruzineService.openCustomDialog('successfully sent.');
      this.contactForm.reset();
      return;
    }
    this.cruzineService.openCustomDialog('not sent. An error occured.');
  }

  async sendEmail(email, message) {
    return await send(
      environment.emailJs.serviceId,
      environment.emailJs.templateId,
      {
        email,
        message,
      },
      environment.emailJs.userId
    );
  }

  openSnackBar() {
    this._snackBar.open('Form is invalid', 'Error', {
      duration: 5000,
      verticalPosition: 'top',
      direction: 'rtl',
    });
  }
}
