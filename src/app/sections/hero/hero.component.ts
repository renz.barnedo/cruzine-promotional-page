import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss'],
})
export class HeroComponent implements OnInit {
  constructor() {}
  toolbarClass = 'toolbar transparent';

  ngOnInit(): void {}

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
    this.toolbarClass = 'toolbar transparent';
    const scrollHeight = window.pageYOffset;
    if (scrollHeight >= 100) {
      this.toolbarClass = 'toolbar solid';
    }
  }
}
