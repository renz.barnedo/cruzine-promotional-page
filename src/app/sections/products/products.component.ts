import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  counter: number = 0;
  folderPath = '../../../assets/';
  products: object[] = [
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: "S'more Cookie",
      subtitle: 'Dessert',
      description:
        'Vegetarian friendly. With slightly roasted marshmallow inside! You can request to exclude the marshmallow.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Strawberry Shortcake',
      subtitle: 'Dessert',
      description:
        '20cm round shortcake perfect for your celebrations, vegetarian edition! Cake design may vary.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Fudgy Brownie',
      subtitle: 'Dessert',
      description:
        'Irresistibly fudgy brownies for your chocolatey cravings! Vegetarian Friendly.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Soy Berry Pink Drink',
      subtitle: 'Drink',
      description:
        'Strawberry milk made with our homemade soymilk and strawberry puree! Naturally flavored. No preservatives added. Kids approve!',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Hazelnut Latte',
      subtitle: 'Drink',
      description:
        'Cold brewed hazelnut coffee beans with our homemade soy milk. Includes simple syrup for your preferred sweetness.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Soy Latte',
      subtitle: 'Drink',
      description:
        'Our signature coffee blend made with cold brewed coffee and homemade soy milk! Includes simple syrup for your preferred sweetness.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Vegetarian Quesadilla',
      subtitle: 'Food',
      description: 'Includes eggs, cheese and vegan meat.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Cheesy Potato Balls',
      subtitle: 'Food',
      description: 'Baked potatoes with cheese and parsley on top.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Meatless Baked Macaroni',
      subtitle: 'Pasta',
      description: 'Vegetarian friendly, No MSG.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Creamy Penne Carbonara',
      subtitle: 'Pasta',
      description: 'Vegetarian friendly, No MSG.',
    },
    {
      unsplash: this.getRandomImage(),
      image: 'cake.jpg',
      title: 'Meatless Cheesy Lasagna',
      subtitle: 'Pasta',
      description: 'Our bestselling pasta! Vegetarian friendly, No MSG.',
    },
  ];

  getRandomImage() {
    const keywords = [
      'cookie',
      'strawberry cake',
      'brownie',
      'berry drink',
      'hazelnut latte',
      'soy latte',
      'quesadilla',
      'cheesy potato',
      'baked macaroni',
      'carbonara',
      'cheesy lasagna',
    ];
    const keyword = keywords[this.counter];
    this.counter++;
    return `https://source.unsplash.com/1600x900/?${keyword}`;
  }

  constructor() {}

  ngOnInit(): void {}
}
