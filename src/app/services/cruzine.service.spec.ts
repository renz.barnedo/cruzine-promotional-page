import { TestBed } from '@angular/core/testing';

import { CruzineService } from './cruzine.service';

describe('CruzineService', () => {
  let service: CruzineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CruzineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
