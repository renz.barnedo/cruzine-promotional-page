import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../utilities/dialog/dialog.component';
import { SpinnerComponent } from '../utilities/spinner/spinner.component';

@Injectable({
  providedIn: 'root',
})
export class CruzineService {
  constructor(private dialog: MatDialog) {}

  openCustomDialog(message) {
    this.dialog.open(DialogComponent, {
      data: {
        message,
      },
    });
  }

  openLoadingDialog() {
    let dialogRef: MatDialogRef<SpinnerComponent> = this.dialog.open(
      SpinnerComponent,
      {
        panelClass: 'transparent',
        disableClose: true,
      }
    );
  }

  closeLoadingDialog() {
    this.dialog.closeAll();
  }
}
