import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SectionsComponent } from './sections/sections.component';
import { HeroComponent } from './sections/hero/hero.component';
import { ProductsComponent } from './sections/products/products.component';
import { ContactComponent } from './sections/contact/contact.component';
import { FooterComponent } from './sections/footer/footer.component';
import { MaterialModule } from './material/material.module';
import { AboutComponent } from './sections/about/about.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpinnerComponent } from './utilities/spinner/spinner.component';
import { DialogComponent } from './utilities/dialog/dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    SectionsComponent,
    HeroComponent,
    ProductsComponent,
    ContactComponent,
    FooterComponent,
    AboutComponent,
    SpinnerComponent,
    DialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [DialogComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
